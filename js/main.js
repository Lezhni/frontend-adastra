$(document).ready(function() {
    $('.second-level').prev('a').click(function() {
        if($(this).hasClass('opened')) {
            $(this).removeClass('opened').next('.second-level').slideUp();
        } else {
            $('.second-level').prev('a').removeClass('opened').next('.second-level').slideUp();
            $(this).addClass('opened').next('.second-level').slideDown();
        }
        return false;
    });
});

var hwSlideSpeed = 700;
var hwTimeOut = 3000;
var hwNeedLinks = false;

$(document).ready(function(e) {
    $('.slide').css(
        {"position" : "absolute",
            "top":'0', "left": '0'}).hide().eq(0).show();
    var slideNum = 0;
    var slideTime;
    var slideCount = $("#slidertop .slide").size();
    var animSlide = function(arrow){
        clearTimeout(slideTime);
        $('.slide').eq(slideNum).fadeOut(hwSlideSpeed);
        if(arrow == "next"){
            if(slideNum == (slideCount-1)){slideNum=0;}
            else{slideNum++}
        }
        else if(arrow == "prew")
        {
            if(slideNum == 0){slideNum=slideCount-1;}
            else{slideNum-=1}
        }
        else{
            slideNum = arrow;
        }
        $('.slide').eq(slideNum).fadeIn(hwSlideSpeed, rotator);
        $("#slider-nav li.active").removeClass("active");
        $("#slider-nav li").eq(slideNum).addClass('active');
    }
    var $adderSpan = '';
    $('.slide').each(function(index) {
        $adderSpan += '<li>'+index+'</li>';
    });
    $('<ul id="slider-nav">'+$adderSpan+'</ul>').appendTo('#slidertop');
    var w = 0;
    $('#slider-nav li').each(function() {
        w += $(this).width() + 5;
    });
    /*$('.slide-block').each(function() {
        $(this).css('width',$(this).width());
    });*/
    $('#slider-nav').css('margin-left',-w/2+'px');
    $("#slider-nav li:first").addClass("active");
    $('#slider-nav li').click(function(){
        var goToNum = parseFloat($(this).text());
        animSlide(goToNum);
    });
    var pause = false;
    var rotator = function(){
        if(!pause){slideTime = setTimeout(function(){animSlide('next')}, hwTimeOut);}
    }
    $('#slider').hover(
        function(){clearTimeout(slideTime); pause = true;},
        function(){pause = false; rotator();
        });
    rotator();
});